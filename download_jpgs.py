#!/usr/bin/env python
"""Script for downloading .jpg files from URL addresses into a specific folder.

First argument: location of plaintext file, containing one URL per newline.
Second argument (optional): Download directory, passed as an absolute path. Default is the current
working directory.
Only urls to jpg files are accepted.

TODO:
    - Fully fail proof naming. Check how plaintext file is created.
    - Possibly circumvent blocking of urllib.retrieve on some Websites.
"""

import sys
import os

# Backwards compatibility with Python 2.x
try:
    from urllib.request import urlretrieve
    from urllib.error import HTTPError
except ImportError:
    from urllib import urlretrieve
    from urllib2 import HTTPError

# Set folder for saving.
if len(sys.argv) == 3:
    folder = sys.argv[2]
else:
    folder = os.getcwd()

with open(sys.argv[1], 'r') as f:
    for line in f.read().splitlines():
        # Create appropriate string for naming. Not all possible error cases on Windows systems
        # are accounted for.
        img_name = os.path.basename(line).replace('?', '').replace(':', '').replace('*', '')
        if not img_name[-4:].lower() == '.jpg':
            img_name += '.jpg'
        # Retrieve and save jpgs.
        try:
            urlretrieve(line, os.path.join(folder, img_name))
        except HTTPError as exc:
            # Check for 'HTTP Error 403: Forbidden', i.e. the Website is most likely blocking the
            # user-agent of urllib. This could be circumvented by setting a different user agent,
            # e.g. 'Mozilla/5.0', however we choose to respect the source settings here.
            # Use logger for issuing the message.
            if exc.code == 403:
                print('HTTP Error 403 at url: {}\n'
                      'Access from urllib might be blocked. Skipping download.'.format(line))
                pass
