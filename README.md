# JPGScraper

A script to download JPG files

## Getting Started

Run the script from the command line using
'python download_jpg.py sample_images.txt path_to_dwnload_folder'
with a plaintext file containing urls as a second and a optional download folder
as a third command.

### Prerequisites

Python 2.x or 3.x

## Versioning

Check the [Gitlab repository](https://gitlab.com/schlipf/JPGScraper.git)
for version updates.

## Authors

[**Lukas Schlipf**](https://gitlab.com/schlipf)

## License

This project is licensed under the GPL License (https://www.gnu.org/licenses/gpl.html)
